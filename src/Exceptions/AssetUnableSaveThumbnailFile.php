<?php

namespace Drupal\ib_dam\Exceptions;

/**
 * Class AssetUnableSaveThumbnailFile.
 *
 * @package Drupal\ib_dam\Exceptions
 */
class AssetUnableSaveThumbnailFile extends AssetUnableSaveLocalFile {}
